from django import forms

class scheduleform(forms.Form):
    error_messages = {
        'required': 'Please fill',
    }

    date_attrs = {
        'type': 'datetime-local',
        'class' : 'form-control',
        'placeholder':'Date of Event'
    }

    ev_attrs = {
        'type' : 'text',
        'class' : 'form-control',
        'placeholder' : 'Event Name'
    }
    
    plc_attrs = {
        'type' : 'text',
        'class' : 'form-control',
        'placeholder' : 'Place'
    }

    cat_attrs = {
        'type' : 'text',
        'class' : 'form-control',
        'placeholder' : 'Category'
    }

    eventname = forms.CharField(label='Event Name', required=True, widget=forms.TextInput(attrs=ev_attrs))
    date = forms.DateField(label='Date and Time', required = True, widget=forms.DateInput(attrs=date_attrs))
    place = forms.CharField(label='Place', required=True, widget=forms.TextInput(attrs=plc_attrs))
    category = forms.CharField(label='Category', required=True, widget=forms.TextInput(attrs=cat_attrs))

