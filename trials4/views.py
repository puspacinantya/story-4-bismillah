from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import scheduleform
from .models import schedulemodel

response = {}

# Create your views here.
def homepage(request):
    return render(request, 'homepage.html')

def newproject(request):
    return render(request, 'newproject.html')

def mywritings(request):
    return render(request, 'mywritings.html')

def testimonials(request):
    return render(request, 'testimonials.html')

def contact(request):
    return render(request, 'contact.html')

def education(request):
    return render(request, 'education.html')

def guestbook(request):
    return render(request, 'bukutamu.html')

def jadwalku(request):
	response['savejadwal'] = scheduleform #savejadwal diambil dari fungsi savejadwal di bawahnya
	html = 'jadwalku.html'
	return render(request, html, response)

def savejadwal(request):
    #form = jadwalku(request.POST or None)
    if(request.method == 'POST'):
        response['eventname'] = request.POST['eventname']
        response['date'] = request.POST['date']
        response['place'] = request.POST['place']
        response['category'] = request.POST['category']
        sched = schedulemodel(date=response['date'],eventname=response['eventname'], place=response['place'], category=response['category'])
        sched.save()
        html = 'jadwalku.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/trials4/schedule')

def tablejadwal(request):
    table = schedulemodel.objects.all()
    response['table'] = table
    html = 'tablejadwalku.html'
    return render(request, html, response)

def deletetable(request):
    delete = schedulemodel.objects.all().delete()
    return HttpResponseRedirect('/trials4/scheduleresult')