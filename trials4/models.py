from django.db import models

# Create your models here.
class schedulemodel(models.Model):
    eventname = models.TextField(max_length=27)
    date = models.DateTimeField()
    place = models.TextField(max_length=27)
    category = models.TextField(max_length=27)

